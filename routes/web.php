<?php

use App\Task;
use App\Cgy;
use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('saveTask',function(){
// 	//$task = new Task();
// 	$task = new Task();
// 	$task->saveTask();
// });

// Route::get('showTask',function(){
// 	$task = new Task();
// 	return $task->showTask(1);
// });

// Route::get('do',function(){
// 	Task::doSomeThing();
// });

// Route::get('newCompany',function(){
// 	$company = new Company();
// 	$company->name = '101大樓';
// 	$company->phone = '119';
// 	$company->save();
// });

//顯示所有任務
Route::get('/', function(){
	return redirect('/task');
});

//變更任務
Route::get('/tasks/modify/{id}',function($id){
	$task = Task::find($id);
	$task->title = 'ss';
	$task->save();
});

// //增加新的任務
// Route::post('/task','TaskController@store');


// //刪除任務
// Route::get('/tasks/del/{id}','TaskController@destroy');

//Route Model Binding示範
// Route::get('/tasks/{task}',function(App\Task $task){
// 	$task->delete();
// });

// Route::get('api/users/{user}', function (App\User $user) {
//     return $user->email;
// });

Route::resource('task','TaskController');

// Route::get('task','TaskController@index');

// Route::get('task/create','TaskController@createTask');

//取得所有輸入資料，要測試請自行開啟
// Route::post('task',function(Request $request){
// 	var_dump($request->all());
// });

//取得部分輸入資料，要測試請自行開啟
// Route::post('task',function(Request $request){
// 	var_dump($request->except('_token')); //某欄位不取得
// 	var_dump($request->only(['title','sub_title'])); //只限定某些欄位
// });

//確認某輸入字段是否存在
// Route::post('task',function(Request $request){

// 	if ($request->exists('title')) {
// 		return '存在';
// 	}else{
// 		return '不存在';
// 	}
	
// });

//取得部分字段內容
// Route::post('task',function(Request $request){
// 	//取得title字段的內容，如果不存在該字段則回傳第二個參數
// 	return $request->input('title','(anonymous)');

// 	//陣列輸入，可透過.以及*來挖掘陣列結構
// 	//return $request->input('tasks.0.title'); //取得第一個title
// 	return $request->input('tasks.*.title'); //取得所有title
// });




//輸出
// [
// 	'page_view' => 10,
// 	'is_feature' => 0,
// 	'title' => 'This is Title',
// 	'sub_title' => 'This is Sub Title',
// 	'content' => 'Content here'
// ]


// Route::get('hello',function(){
// 	return 'Hello';
// });


//路由規則示範

//轉址到外部網址
Route::get('/baha',function(){
	return redirect('http://www.gamer.com.tw');
});

//轉址到內部網址
Route::get('/redirect',function(){
	return redirect(url('hello'));
});

//建立由Controller處理的規則
Route::get('/home','HomeController@index');

//回傳視圖
Route::get('/gamer',function(){
	$title = '冬日水槍戰';
	return view('gamer',compact('title'));
	//return view('gamer')->with('title','冬日水槍戰');
});

//回傳子資料夾的視圖
Route::get('/gamer1',function(){
	return view('pages.gamer1');
});
Route::get('/gamer2',function(){
	return view('pages.gamer2');
});
Route::get('/gamer3',function(){
	return view('pages.gamer3');
});

//傳入特定參數
// Route::get('hello/{name}',function($name){
// 	return 'Hello,' . $name;
// });

Route::get('dashboard',function(){
	return view('dashboard');
});


//將規則放在同一個群組裡頭
Route::group([],function(){
	// //回傳字串
	Route::get('/hello', function() {
		return 'Hello World';
	});

	//傳入可給可不給的參數
	Route::get('hello/{from?}/{to?}',function($from = 'Everybody',$to = 'Taiwan'){
		return 'Hello, From ' . $from . ' to ' . $to;
	});
});



//為路由規則建立名稱
// Route::get('users','UsersController@index')->name('users.index');

Route::get('test-artisan',function(){
	$exitCode = Artisan::call('migrate', []);
	return $exitCode;
});

Route::get('getParams','HomeController@getParams');





//API示範
//直接在router回傳JSON字串
Route::group(['prefix' => 'api'],function(){
	// //回傳字串
	Route::get('tasks/',function(){
		return Task::take(10)->get()->toJson(); 
	});
});

//關係示範
Route::group(['prefix' => 'ref'],function(){
	//一對一關係
	Route::get('/cgyTitle/{task_id}', function($task_id) {
		return Task::find($task_id)->cgy->title;
	});
	//一對多關係
	Route::get('/tasks/{cgy_id}', function($cgy_id) {
		return Cgy::find($cgy_id)->tasks;
	});
	//插入一個新項目到一對多關係
	Route::get('cgys/addTask',function(){
		$task = new Task;
		$task->title = '關聯Title';
		$task->sub_title = '關聯sub_title';
		$task->content = '關聯content';
		$task->page_view = 1;
		$task->is_feature = 0;
		$cgy = Cgy::first();
		$cgy->tasks()->save($task);

	});
	//插入多個新項目到一對多關係
	Route::get('cgys/addTasks',function(){
		$task = new Task;
		$task->title = '多新項目1Title';
		$task->sub_title = '多新項目1sub_title';
		$task->content = '多新項目1content';
		$task->page_view = 1;
		$task->is_feature = 0;
		$task2 = new Task;
		$task2->title = '多新項目2Title';
		$task2->sub_title = '多新項目2sub_title';
		$task2->content = '多新項目2content';
		$task2->page_view = 0;
		$task2->is_feature = 1;
		$cgy = Cgy::first();
		$cgy->tasks()->saveMany([$task,$task2]);

	});
	//附加相關項目
	Route::get('task/modifyAssocCgy',function(){
		$task = Task::find(20003);
		$cgy = Cgy::find(107);
		$task->cgy()->associate($cgy);
		$task->save();
	});
	//移除相關項目，會將外鍵的值設為空
	Route::get('task/modifyDissocCgy',function(){
		$task = Task::find(20003);
		$task->cgy()->dissociate();
		$task->save();
	});
	//只選擇有相關項目的紀錄
	Route::get('cgys/hasDemo',function(){
		//$cgys = Cgy::has('tasks')->get();
		$cgys = Cgy::has('tasks','>=',2500)->get();
		dd($cgys);
	});
	//多對多關係
	Route::get('users/hasTasks',function(){
		$user1 = User::find(10);
		$task1 = Task::find(1);
		$user1->tasks()->save($task1);
		$user1->tasks()->attach(2);
		$user1->tasks()->attach([3,4]);
	});
	Route::get('users/removeTasks',function(){
		$user1 = User::find(10);
		$user1->tasks()->detach(1);
	});
	Route::get('users/syncTasks',function(){
		$user1 = User::find(10);
		$user1->tasks()->sync([1,3,5,7,9]);
	});
	//取得多對多資料
	Route::get('user/queryTasks',function(){
		$user1 = User::first();
		dd($user1->tasks);
	});
	Route::get('task/queryUsers',function(){
		$task1 = Task::find(20002);
		dd($task1->users);
	});
	//積極載入
	Route::get('user/withTasks',function(){
		$cgys = Cgy::with('tasks')->get(); //在查詢所有Cgy資料時一併拉出關聯的Task資料
		$cgys = Cgy::withCount('tasks')->get(); //在查詢所有Cgy資料時一併計算關聯的Task數量
		dd($cgys);
	});
	
	
});

Route::get('/test/carbon',function(){
	return Carbon\Carbon::now()->subDay(5)->diffForHumans();
});

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
