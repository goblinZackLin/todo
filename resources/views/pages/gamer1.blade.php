
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#">
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<title>Game1</title> 
<meta name="keywords" content="《閃亂神樂 夏日對決 -少女們的抉擇-》PC 繁體中文版即將在台發行《SENRAN KAGURA ESTIVAL VERSUS》">
<meta name="Description" content="英特衛今（2）日宣布，取得日本開發商 Marvelous 旗下動作遊戲《閃亂神樂 夏日對決 -少女們的抉擇-》在台發行權，並將推出 PC 繁體中文版，上市日期將於近期內公布。 《閃亂神樂》系列遊戲是款融入了變身與爆衣要素、描寫為忍者之路賭上人生的少女們熱血激烈戰鬥的動作遊戲。在前作 《閃亂神樂 忍者對決 - 少女們的証明 -》中，「死">
<meta property="og:site_name" content="巴哈姆特電玩資訊站">
<meta property="fb:app_id" content="668497826514848">
<meta property="og:description" content="英特衛今（2）日宣布，取得日本開發商 Marvelous 旗下動作遊戲《閃亂神樂 夏日對決 -少女們的抉擇-》在台發行權，並將推出 PC 繁體中文版，上市日期將於近期內公布。 《閃亂神樂》系列遊戲是款融入了變身與爆衣要素、描寫為忍者之路賭上人生的少女們熱血激烈戰鬥的動作遊戲。在前作 《閃亂神樂 忍者對決 - 少女們的証明 -》中，「死"/>
<link href="https://i2.bahamut.com.tw/css/BH.css" rel="stylesheet" type="text/css" />
<link href="https://i2.bahamut.com.tw/css/search.css" rel="stylesheet" type="text/css" />
<link href="https://i2.bahamut.com.tw/css/index_top.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://i2.bahamut.com.tw/css/font-awesome.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" href="http://m.gamer.com.tw/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="72x72" href="http://m.gamer.com.tw/apple-touch-icon-72x72.png" /> 
<link rel="apple-touch-icon" sizes="144x144" href="http://m.gamer.com.tw/apple-touch-icon-144x144.png" />
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/sys.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/BAHA_EGG.js"></script>
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/dreamweaver.js"></script>
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/rsearch.js"></script>
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/BH_topBar.js"></script>
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/BH_mainmenu.js"></script>
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/ad.js"></script>
<script type="text/javascript" src="https://i2.bahamut.com.tw/JS/ad/insideAD.js?v=2017060310"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
if( typeof BH_AlkasooD === 'function' ) {
BH_AlkasooD($);
}
});
BAHAID = getCookie( 'BAHAID' );
if(BAHAID) {
notifynum = "";
}
//anti adblock
var AntiAd = AntiAd || {};
(function(AntiAd) {
'use strict';
AntiAd.flag = false;
AntiAd.whiteList = [];
AntiAd.verifyLink = function (link) {}
AntiAd.block = function() {}
AntiAd.check = function(element) {}
})(AntiAd);
</script>
<link rel="stylesheet" type="text/css" href="https://i2.bahamut.com.tw/css/gnn_2k13.css" />
<link rel="alternate" type="application/rss+xml" title="巴哈姆特 GNN 新聞網" href="https://gnn.gamer.com.tw/rss_utf8.xml" />
<script type="text/javascript" src="https://i2.bahamut.com.tw/js/gnn_new.js"></script>
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.gamer.com.tw/gnn/detail.php?sn=147807">
<meta property="og:image" content="https://p2.bahamut.com.tw/B/2KU/53/0001553753.JPG" />
<meta name="thumbnail" content="https://p2.bahamut.com.tw/B/2KU/53/0001553753.JPG" />
<meta property="og:url" content="https://gnn.gamer.com.tw/7/147807.html" />
<meta property="og:title" content="《閃亂神樂 夏日對決 -少女們的抉擇-》PC 繁體中文版即將在台發行" />
<meta property="og:type" content="article" />
<meta property="al:ios:url" content="bahamut://gnnDetail?sn=147807" />
<meta property="al:ios:app_store_id" content="479826450" />
<meta property="al:ios:app_name" content="巴哈姆特" />
<script type="application/ld+json">[{"@context":"http:\/\/schema.org","@type":"WebSite","name":"\u5df4\u54c8\u59c6\u7279\u96fb\u73a9\u8cc7\u8a0a\u7ad9","url":"https:\/\/www.gamer.com.tw\/","alternateName":"\u5df4\u54c8\u59c6\u7279"},{"@context":"http:\/\/schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"item":{"@id":"https:\/\/gnn.gamer.com.tw\/","name":"GNN \u65b0\u805e"}},{"@type":"ListItem","position":2,"item":{"@id":"https:\/\/gnn.gamer.com.tw\/7\/147807.html","name":"\u300a\u9583\u4e82\u795e\u6a02 \u590f\u65e5\u5c0d\u6c7a -\u5c11\u5973\u5011\u7684\u6289\u64c7-\u300bPC \u7e41\u9ad4\u4e2d\u6587\u7248\u5373\u5c07\u5728\u53f0\u767c\u884c"}}]}]</script>
<script src="https://i2.bahamut.com.tw/js/plugins/imagegallery.js"></script>
<script src="https://i2.bahamut.com.tw/js/plugins/lazysizes-1.3.1.min.js" async></script>
<script src="https://i2.bahamut.com.tw/js/bookmark.js"></script>
<script src="https://i2.bahamut.com.tw/js/home_acg.js"></script>
<script src="https://i2.bahamut.com.tw/js/movetomobile.js"></script>
<link href="https://i2.bahamut.com.tw/css/acg_new.css" rel="stylesheet" type="text/css" />
<script>
BAHAID = getCookie( 'BAHAID' );
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-108920-1', 'auto');
if( BAHAID ){
ga('set', 'userId', BAHAID.toLowerCase());
}
ga('send', 'pageview');
</script><script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
</script>
<script>
bahalv = getCookie('BAHALV');
bahalv = (bahalv) ? bahalv : 0;
var fparam = location.search;
var bsnary = fparam.match(/\d+/g);
var gpt_bsn = (bsnary === null) ? 0 : bsnary[0];
googletag.cmd.push(function() {googletag.defineSlot('/1017768/B300', [[300, 250], [300, 600]], 'div-gpt-ad-1489070677458-0').addService(googletag.pubads());googletag.pubads().setTargeting("LV", bahalv);
googletag.pubads().setTargeting("bsn", gpt_bsn);googletag.pubads().setTargeting("service","gnn");
googletag.pubads().enableSingleRequest();
googletag.pubads().collapseEmptyDivs();
googletag.enableServices();
});
</script></head> 
<body> 
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KDKMGT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KDKMGT');</script>
<!-- End Google Tag Manager --><div id="fb-root"></div> <script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1&appId=668497826514848"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script>
<div class="TOP-bh"><!--全站內頁天-->
<div class="TOP-data" id="BH-top-data"><!--全站內頁天中間區塊-->
<script type="text/javascript">
if(BAHAID){
BAHAIDlow = BAHAID.toLowerCase();
document.write('<div class="TOP-my">');
document.write('<ul>');
document.write('<li><a id="topBar_mailbox" href="https://mailbox.gamer.com.tw/"><img src="https://i2.bahamut.com.tw/top_icon3.png"></a></li>');
document.write('<li><a id="topBar_guild" href="javascript:TOPBAR_show(\'guild\', \'\')"><img src="https://i2.bahamut.com.tw/top_icon1.png"></a></li>');
document.write('<li><a id="topBar_forum" href="javascript:TOPBAR_show(\'forum\', \'\')"><img src="https://i2.bahamut.com.tw/top_icon4.png"></a></li>');
document.write('<li><a href="https://home.gamer.com.tw/'+BAHAID+'"><img src="https://avatar2.bahamut.com.tw/avataruserpic/'+BAHAIDlow.substr(0,1) +'/'+BAHAIDlow.substr(1,1)+'/'+BAHAIDlow+'/'+BAHAIDlow+'_s.png?x=201706031000" /></a></li>');
document.write('<li><a id="topBar_more" style="font-size:11px;" href="javascript:TOPBAR_show(\'more\', \'\')"><img src="https://i2.bahamut.com.tw/menu.gif"></a></li>');
document.write('</ul>');
document.write('</div>');
document.write('<p class="TOP-btn">');
document.write('<a href="javascript:TOPBAR_show(\'light_0\', \'topb1\')" id="topBar_light_0" class="topb1"></a>');
document.write('<a href="javascript:TOPBAR_show(\'light_1\', \'topb2\')" id="topBar_light_1" class="topb2"></a>');
document.write('<a href="javascript:TOPBAR_show(\'light_2\', \'topb3\')" id="topBar_light_2" class="topb3"></a>');
document.write('</p>');
//document.write('<div id="topBarMsg_more" style="display:none"></div>');
document.write('<div id="topBarMsg_forum" style="display:none"></div>');
document.write('<div id="topBarMsg_guild" style="display:none"></div>');
document.write('<div id="topBarMsg_light_0" style="display:none"></div>');
document.write('<div id="topBarMsg_light_1" style="display:none"></div>');
document.write('<div id="topBarMsg_light_2" style="display:none"></div>');
}
else{
document.write('<div class="TOP-my TOP-nologin">');
document.write('<a href="https://user.gamer.com.tw/login.php"><img src="https://i2.bahamut.com.tw/none.gif" /></a>');
document.write('<ul>');
document.write('<li><a href="https://user.gamer.com.tw/login.php">我要登入</a></li>');
document.write('<li><a href="https://user.gamer.com.tw/regS1.php">註冊</a></li>');
document.write('<li><a id="topBar_more" style="font-size:11px;" href="javascript:TOPBAR_show(\'more\', \'\')"><img src="https://i2.bahamut.com.tw/menu.gif"></a></li>');
document.write('</ul>');
document.write('</div>');
}
document.write('<div id="topBarMsg_more" style="display:none"></div>');
var googlecseLoad = function() {
var cx = 'partner-pub-9012069346306566:kd3hd85io9c';
var gcse = document.createElement('script');
gcse.type = 'text/javascript';
gcse.async = true;
gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(gcse, s);
};
document.write('<a href="https://www.gamer.com.tw/"><img src="https://i2.bahamut.com.tw/top_logo.png?v=2013100402" style="height:35px;"/></a><p class="TOP-search"><gcse:searchbox-only resultsUrl="https://search.gamer.com.tw"></gcse:searchbox-only></p>');
service = new rsearch('rsearch');
if( BAHAID ) {
NOTIFY_getnum();
run30 = setInterval("NOTIFY_getnum()",60000);
}
function insideSecondaryfunc(frm, evt){
if( 0 < egg('.isecondaryfunc').size() ){
var class_display_state = egg('.isecondaryfunc').css('display');
if( "none" == class_display_state ){
if( 1 == frm  ){
egg('.isecondaryfunc').css('display', '');
egg('#isecondaryfuncshow').css('display', 'none');
}
}else{
egg('.isecondaryfunc').css('display', 'none');
egg('#isecondaryfuncshow').css('display', '');
}
evt = window.event || evt;
egg.event.stop(evt);
}
}
</script>
</div><!--全站內頁天中間區塊結束-->
</div><!--全站內頁天結束-->
<div id="BH-background">
<div id="BH-wrapper" style="margin-top:35px;"><!--最外層寬1000包裝-->
<div id="BH-bigbanner" align="center">
<script type='text/javascript'>top_abanner1000()</script>
</div>
<!--全站主選單-->
<div id="BH-menu-fill">&nbsp;</div>
<div id="BH-menu-path" class="BH-menu">
<script type="text/javascript">
var searchParam = {
form: {
name: 'forum_search',
method: 'get',
action: '//acg.gamer.com.tw/search.php'
},
input: [
{ name: 's', type: 'hidden', value: '2'},
{ id: 'search_text', name: 'kw', type: 'text', value: "" }
],
select: {
option: [
{ text: '找新聞', value: 1 }
],
onchange: 'egg(\'#searchtxt\').html((egg(\'[name=stype]\').find(\':selected\').html()));'
},
searchBtn: { onclick: 'document.forum_search.submit();'},
stext: "",
sval: ""
};BH_Menu_Render('GNN', 0, 0, '', searchParam);
</script>
</div>
<!--全站主選單結束-->
<div style="width:0;height:0;position:relative;top:0px;left:0px;z-index:100;"><div style="position:absolute; top:0px; left:730px; width:268px; border: solid 1px #000000; background: #FFFFFF; z-index:100;display:none;" class="rsearch" id="rsearch" align="left"><ol id="suggestol"></ol></div></div><!--搜尋BAR下拉結束-->
<!--服務選單-->
<div id="BH-master"><!--內容左側區塊--><div class="BH-lbox GN-lbox3" style="overflow:visible;"><!--區塊1開始-->
<div class="fb-like" data-href="https://gnn.gamer.com.tw/7/147807.html" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
<h1 style="width: 585px;"><img src="https://i2.bahamut.com.tw/spacer.gif" class="IMG-GA19" title="PC單機遊戲">{{$title}}</h1>
<div class="GN-lbox3B"><!--新聞內容開始-->
<span class="GN-lbox3C">（GNN 記者 Jessica 報導） 2017-06-03 10:00:00</span>
<p>
<img class="GN-lbox3D" src="https://p2.bahamut.com.tw/B/2KU/52/0001553752.JPG" style="height: 96px; width: 150px;" alt="image" /></p>
<div>
<div>
　　{{$content}}</div>
<div>
&nbsp;</div>

<div>
&nbsp;</div>
<div>
<ul class="GN-thumbnails">
<li>
<div class="GN-thumbnail">
<img  name="gnnPIC" class="lazyload" data-sizes="auto" data-src="https://p2.bahamut.com.tw/M/2KU/47/0001553747.JPG" style="width:640px; height:361.5267175572519px;" title="" alt="image" /></div>
</li>
<li>
<div class="GN-thumbnail">
<img  name="gnnPIC" class="lazyload" data-sizes="auto" data-src="https://p2.bahamut.com.tw/M/2KU/51/0001553751.JPG" style="width:640px; height:401.3559322033898px;" title="" alt="image" /></div>
</li>
<li>
<div class="GN-thumbnail">
<img  name="gnnPIC" class="lazyload" data-sizes="auto" data-src="https://p2.bahamut.com.tw/M/2KU/49/0001553749.JPG" style="width:640px; height:401.3559322033898px;" title="" alt="image" /></div>
</li>
<li>
<div class="GN-thumbnail">
<img  name="gnnPIC" class="lazyload" data-sizes="auto" data-src="https://p2.bahamut.com.tw/M/2KU/50/0001553750.JPG" style="width:640px; height:401.3559322033898px;" title="" alt="image" /></div>
</li>
<li>
<div class="GN-thumbnail">
<img  name="gnnPIC" class="lazyload" data-sizes="auto" data-src="https://p2.bahamut.com.tw/M/2KU/48/0001553748.JPG" style="width:640px; height:361.5267175572519px;" title="" alt="image" /></div>
</li>
</ul>
</div>
<div>
&nbsp;</div>
<div>
　　故事一開始，「秘立蛇女子學園」的兩備、兩奈在森林掃墓時，無意間目擊到一個神祕的儀式，她們死去的姊姊兩姬復活了......這就是長達八天，滿載夏天、海邊、慶典、泳裝、忍術，以「神樂千年祭」為舞台展開精彩的夏日祭典故事之開端......</div>
<div>
&nbsp;</div>
<div>
<iframe  allowfullscreen="" frameborder="0" height="360" class="lazyload" data-sizes="auto" data-src="https://www.youtube.com/embed/S6AfRFtt4Sk?wmode=transparent" width="640"></iframe></div>
<div>
&nbsp;</div>
<div>
　　《<a class="acglink" href="//acg.gamer.com.tw/search.php?encode=utf8&amp;kw=%E9%96%83%E4%BA%82%E7%A5%9E%E6%A8%82+%E5%A4%8F%E6%97%A5%E5%B0%8D%E6%B1%BA+-%E5%B0%91%E5%A5%B3%E5%80%91%E7%9A%84%E6%8A%89%E6%93%87-" target="_blank">閃亂神樂 夏日對決 -少女們的抉擇-</a>》即將在台推出 PC 繁體中文版，官方透露，首發版本將限量推出「少女們的私密手冊～畫冊 ‧ 原聲帶套組～」，內含畫冊和原聲帶 CD 的超豪華特典套組。</div>
</div>
<p style="font-size:12px;padding:10px 0;"></p>
</div><!--新聞內容結束-->
<p class="GN-lbox3E"></p><!--相關連結-->
</div><!--區塊1結束-->
<div class="GN-lbox5"><!--功能區塊-->
<ul class="GN-lbox5A">
<li>
<a href="//forum.gamer.com.tw/searchb.php?dc_c1=1200&dc_c2=10&dc_type=1&dc_machine=262144,0"><i class="fa fa-comments"></i>哈啦板</a>
</li><li>
<a href="javascript:baha_bookmark()"><i class="fa fa-tag"></i>收藏</a>
</li>
<li>
<a href="//acg.gamer.com.tw/search.php?c1=1200&c2=10"><i class="fa fa-file-text"></i>作品資料</a>
</li></ul>
<div class="GN-lbox5B">
<p class="track" id="track"></p><div class="fb-like" data-href="https://gnn.gamer.com.tw/7/147807.html" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
</div>
<p style="clear:both;"></p></div><!--功能區塊結束-->
<h4><img src="https://i2.bahamut.com.tw/h1_img.gif" />新聞評語</h4>
<div class="BH-lbox GN-lbox6" id="comment">
<center>載入中...</center>
</div>
<h4><img src="https://i2.bahamut.com.tw/h1_img.gif" />相關新聞</h4>
<div class="BH-lbox GN-lbox9">
<a href="//gnn.gamer.com.tw/5/147575.html" data-gtmgnn="相關新聞"><i class="fa fa-caret-right"></i>《閃亂神樂 桃色海灘戲水大戰》將和動畫《一騎當千》展開合作企畫</a><a href="//gnn.gamer.com.tw/7/147507.html" data-gtmgnn="相關新聞"><i class="fa fa-caret-right"></i>《閃亂神樂 桃色海灘戲水大戰》繁體中文版決定於 2017 年 6 月 27 日上市</a><a href="//gnn.gamer.com.tw/3/145803.html" data-gtmgnn="相關新聞"><i class="fa fa-caret-right"></i>「閃亂神樂」X「HD 震動」？Nintendo Switch《忍紓壓 -閃亂神樂-（暫稱）》曝光</a><a href="//gnn.gamer.com.tw/9/145629.html" data-gtmgnn="相關新聞"><i class="fa fa-caret-right"></i>《閃亂神樂 桃色海灘戲水大戰》最新 DLC 開賣並公開下波更新追加內容</a><a href="//gnn.gamer.com.tw/3/145423.html" data-gtmgnn="相關新聞"><i class="fa fa-caret-right"></i>寶刀未老！《閃亂神樂 桃色海灘戲水大戰》最胸之忍「小百合」強制下載登場</a><span class="keyword-item">關鍵字：<a class="keyword" href="//acg.gamer.com.tw/search.php?s=4&kw=%E9%96%83%E4%BA%82%E7%A5%9E%E6%A8%82">閃亂神樂</a><a class="keyword" href="//acg.gamer.com.tw/search.php?s=4&kw=%E9%96%83%E4%BA%82%E7%A5%9E%E6%A8%82+%E5%A4%8F%E6%97%A5%E5%B0%8D%E6%B1%BA+-%E5%B0%91%E5%A5%B3%E5%80%91%E7%9A%84%E6%8A%89%E6%93%87-">閃亂神樂 夏日對決 -少女們的抉擇-</a></span><p class="GN-lbox8B"><button onclick="location.href='//acg.gamer.com.tw/search.php?s=4&kw=%E9%96%83%E4%BA%82%E7%A5%9E%E6%A8%82%2C%E9%96%83%E4%BA%82%E7%A5%9E%E6%A8%82+%E5%A4%8F%E6%97%A5%E5%B0%8D%E6%B1%BA+-%E5%B0%91%E5%A5%B3%E5%80%91%E7%9A%84%E6%8A%89%E6%93%87-'">看更多</button></p>
</div>
<div id="headnews"></div>
<div id="shop"></div>
</div><!--內容左側區塊結束-->
<div id="BH-slave"><!--右側區塊-->
<div id="flySalve" style="width:300px;z-index:10;top:35px;">
<div style="z-index:8;position:relative;">
<div id='flySalve'><!-- /1017768/B300 -->
<div id='div-gpt-ad-1489070677458-0'>
<script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1489070677458-0'); });</script>
</div></div>
</div>
</div>
</div><!--右側/全版區塊結束-->
<br class="clearfloat" /><!--清除浮動-->
<div id="BH-footer"><!--最底版權宣告-->
<p>本站所刊載之圖文內容等版權皆屬原廠商或原作者所有，非經同意請勿轉載<br />
巴 哈 姆 特 電 玩 資 訊 站 https://www.gamer.com.tw
</p>
</div><!--最底版權宣告結束-->
</div>
<!--最外層寬1000包裝結束-->
</div>
<div
style="
position: fixed;
left: 20px;
right: 20px;
bottom: 20px;
z-index: 99;
display: none;
padding: 15px 30px 15px 15px;
border: 1px solid transparent;
border-radius: 3px;
line-height: 1.5;
background: #FFFFDB;
border-color: #E1D3B8;
color: #705930;">
<a href="javascript:;" class="alert-close">
<i class="fa fa-times"></i>
</a>
<p style="color: #705930;"><i class="material-icons" style="vertical-align: bottom; font-size: 22px; margin-right: 5px;">face</i>我們了解您不想看到廣告的心情⋯ 若您願意支持巴哈姆特永續經營，請將 gamer.com.tw 加入廣告阻擋工具的白名單中，謝謝 !<a href="https://goo.gl/PmkwWS" style="color: #0055aa;" target="_blank;">【教學】</a></p>
</div>
<script>
googlecseLoad();
jQuery(".a-mercy-d").click(function() {
var url = this.href;
var goRedirect = false;
var openNew = (this.target) ? true : false;
if( url ) {
jQuery.get( "/ajax/baha_lkasoo.php", { id: jQuery(this).data('ssn') } )
.done(function() {
goRedirect = true;
if( !openNew ){
document.location = url;
}
});
setTimeout(function() {
if (!goRedirect) {
if( !openNew ){
document.location = url;
}
}
}, 1500);
}
if( !openNew ){
return false;
}
});
var mercyadblock = (function () {
var adblock = {};
adblock.show = function () {
jQuery('.alert-close').parent().show();
};
adblock.hide = function () {
jQuery('.alert-close').parent().hide();
};
adblock.check = function () {
if (localStorage.getItem('admercyblocks') === null) {
jQuery('.alert-close').parent().show();
} else {
var hourts = Math.floor(Date.now() / 1000)-86400;
if (localStorage.getItem('admercyblocks') < hourts) {
jQuery('.alert-close').parent().show();
localStorage.removeItem('admercyblocks');
}
}
};
return adblock;
} ());
if (typeof(Storage) === "undefined") {
/*load fail*/
if (typeof antiadb === 'undefined') {
mercyadblock.show();
} else {
/*load success but open*/
antiadb.onDetected(function () {
mercyadblock.show();
});
}
} else {
/*load fail*/
if (typeof antiadb === 'undefined') {
mercyadblock.check();
} else {
/*load success but open*/
antiadb.onDetected(function(){
mercyadblock.check();
});
}
}
jQuery(".alert-close").click(function() {
mercyadblock.hide();
if (typeof(Storage) !== "undefined") {
localStorage.setItem('admercyblocks', Math.floor(Date.now() / 1000));
}
});
$('#BH-background').click(function(event){
$('#topBarMsg_'+topBarType).css('display','none');
$('#topBar_'+topBarType).attr('className', topBarClass);
insideSecondaryfunc(2, event);
});
var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
if(document.getElementById('BH-top-data').attachEvent){ //if IE (and Opera depending on user setting)
document.getElementById('BH-top-data').attachEvent("on"+mousewheelevt, TOPBAR_stopWheel);
}
else if(document.getElementById('BH-top-data').addEventListener){ //WC3 browsers
document.getElementById('BH-top-data').addEventListener(mousewheelevt, TOPBAR_stopWheel, false);
}
if( 'function' == typeof(chpagehotkey) ){
document.onkeydown = function (evt){
evt = window.event || evt;
chpagehotkey(evt.keyCode);
};
}
if( typeof BH_mainmenu_init === 'function' ){
BH_mainmenu_init();
}
</script>
</body>
</html>
<script>
/****右邊固定****/
function fadRefresh()
{
var flySlave_scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
var flySlave_scrollLeft = window.pageXOffset || document.documentElement.scrollLeft|| document.body.scrollLeft || 0;
var flySlave_clientWidth = document.documentElement.clientWidth|| document.body.clientWidth || 0;
if( 1000 > flySlave_clientWidth ){
var flySlavedis = flySlave_scrollTop+35;
}else{
var flySlavedis = flySlave_scrollTop+70;
}
if( flySlavedis > posY )
{
fad_style.position = 'fixed';
fad_style.top = '72px';
if( 1000 > flySlave_clientWidth ) {
fad_style.left = 700-flySlave_scrollLeft+'px';
fad_style.top = '37px';
}
}else{
fad_style.position = '';
}
}
var posY = egg('#flySalve').pos().y;
var fad_style = document.getElementById("flySalve").style;
window.onscroll = fadRefresh;
window.onresize = function() { fad_style.left = null;};
/****右邊固定****/
</script>
<script>
$('[name=gnnPIC]').imageGallery({'copyrightText':''});
$.ajax({
url:'/ajax/gnn-html.php',
success:function(xmldoc){
var shop = xmldoc.getElementsByTagName('shop');
if( undefined != shop[0] ){
document.getElementById('shop').innerHTML = shop[0].textContent;
}
var headnews = xmldoc.getElementsByTagName('headnews');
if( undefined != headnews[0] ){
document.getElementById('headnews').innerHTML = headnews[0].textContent;
}
var comment = xmldoc.getElementsByTagName('comment');
document.getElementById('comment').innerHTML = comment[0].textContent;
var acg = xmldoc.getElementsByTagName('acg'),
track = document.getElementById('track');
if( undefined != acg[0] && track !== null) {
track.innerHTML = acg[0].textContent;
}
var liveShow = egg('liveShow', xmldoc);
if(liveShow.size()){
var data = [];
var ary = liveShow.get();
for(var i = 0;i < ary.length;i++){
data[ary[i].getAttribute('sn')] = ary[i].textContent;
}
gnn.addShowTab(data);
}
},
param:'sn=147807&c1=1200&c2=10&type=1&machine=262144,0&acg_sn[72672]=72672_1_0,1&acg_sn[72673]=72673_1_1073741824,0&acg_sn[87509]=87509_1_262144,0&category=0&property=0',
method:'GET'
});
buttonToMobile();
</script>
