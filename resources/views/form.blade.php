@extends('layouts.app')

@section('title')
 基本2
@stop

@section('content')
    <style>
        form {
            display: inline-block; //Or display: inline; 
            }
    </style>

    <!-- Bootstrap 樣板... -->

    <div class="panel-body">
        <!-- 顯示驗證錯誤 -->
        @include('common.errors')

        {!! Form::model($task, ['url' => 'task/'.$task->id , 'method' => 'patch' , 'files' => true]) !!}
            {{ csrf_field() }}
            {!! Form::label('title', '標題') !!}
            {!! Form::text('title', null) !!}

            {!! Form::label('sub_title', '副標題') !!}
            {!! Form::text('sub_title', null) !!}<br>

            {!! Form::label('content', '內容') !!}
            {!! Form::textArea('content', null) !!}<br>

            {!! Form::label('page_view', '瀏覽量') !!}
            {!! Form::number('page_view', null,['min'=>0,'max'=>100]) !!}<br>

            {!! Form::label('is_feature', '是否精選') !!}
            {!! Form::checkbox('is_feature', true) !!}<br>

            {!! Form::select('cgy[]', $cgys, null , ['placeholder' => '請選擇分類'],['multiple'=>true]) !!}


            {{--  {!! Form::label('email', '電子郵箱') !!}
            {!! Form::email('email', null) !!}<br>

            {!! Form::label('habits[]', '打球') !!}
            {!! Form::checkbox('habits[]', 'playBall') !!}
            {!! Form::label('habits[]', '看電影') !!}
            {!! Form::checkbox('habits[]', 'movie') !!}
            {!! Form::label('habits[]', '聽音樂') !!}
            {!! Form::checkbox('habits[]', 'Music') !!}<br>

            {!! Form::label('file', '檔案上傳') !!}
            {!! Form::file('file', null) !!}<br>

            {!! Form::label('date', '日期') !!}
            
            {!! Form::date('date', null) !!}
            {!! Form::time('time', null) !!}
            
            {!! Form::datetime('datetime', Carbon\Carbon::now()) !!}  --}}
            
            

            {!! Form::submit('送出') !!}
            
        {!! Form::close() !!}
        
        
    </div>
@stop