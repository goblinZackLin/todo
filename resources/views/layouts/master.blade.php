<!DOCTYPE html>
<html lang="en">
<head>
    <title>ToDo Site | @yield('title','Home')</title>
</head>
<body id="app-layout">
    <div class="container">
        @yield('content')
    </div>
    @include('include',['text' => 'Master Include'])
    @section('footerScripts')
        <script src="app.js"></script>
    @show
</body>
</html>