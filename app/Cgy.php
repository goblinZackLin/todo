<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cgy extends Model
{
    protected $table = 'cgys';

    public function tasks(){
    	return $this->hasMany(Task::class);
    }
}
