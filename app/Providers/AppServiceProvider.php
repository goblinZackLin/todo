<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //處理5.4常见错误：Specified key was too long
        Schema::defaultStringLength(191);

        //所有視圖共用變數
        view()->share('global','ToDo Global'); 

        //讓陣列內的view視圖共用變數
        view()->composer(['partials.header','partials.footer'],function($view){
            $view->with('tasks',Task::all());
        });

        // view()->composer(['pages.gamer1','pages.gamer2'],function($view){
        //     $view->with('title','哥布林學苑新作-Girl Faction即將上市!');
        //     $view->with('content','Girl Faction是一款讓腐男腐女都臉紅心跳的遊戲');
        // });

        view()->composer('pages.*',function($view){
            $view->with('title','哥布林學苑新作-Girl Faction即將上市!');
            $view->with('content','Girl Faction是一款讓腐男腐女都臉紅心跳的遊戲');
        });

        //讓partials資料夾內的所有視圖共用變數
        view()->composer('partials.*',function($view){
            $view->with('tasks',Task::all());
        });

        //自訂指令
        Blade::directive('cust',function(){
            return "<?php echo 'customize directive' ?>";
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //將ModelFactory的Faker語系轉為指定本地化
        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('zh_TW');
        });
    }
}
