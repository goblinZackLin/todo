<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cgy;

class Task extends Model
{

	//建構子，在生成物件時所需要先執行的操作
	// function __construct(){
	// 	dd('建構子');
 //    	$this->title = ''; //$this代表物件本身
 //    }

 //    //建構子
	// function __construct($title){
	// 	dd('建構子 with title');
 //    	$this->title = $title; //$this代表物件本身
 //    }
    

	//預設主鍵為id，可透過以下程式碼來變更
    //protected $primaryKey = 'task_id';

    //將主鍵改為非遞增
    //protected $incrementing = false;

	//回傳JSON只顯示的欄位
    protected $visible = ['title','sub_title','content'];
    //回傳JSON只隱藏的欄位
    protected $hidden = ['page_view','is_feature','created_at','updated_at'];

    //屬性轉換型態
    protected $casts = [
    	'title' => 'string',
    	'sub_title' => 'string',
    	'content' => 'string',
    	'page_view' => 'int',
    	'is_feature' => 'boolean'
    ];

	protected $fillable = ['title','sub_title','content','page_view','is_feature','cgy_id'];

	//當子項目Task發生變更時，父Cgy也跟著變更updated_at欄位
	protected $touches = ['cgy'];

	// static 靜態 不需要建立物件 只會有一份，屬於class
	// public 需要建立物件 有多份 任何class都能取用
	// protected 需要建立物件 有多份 繼承的子class才能取用
	// private 需要建立物件 有多份 只有class自己才能取用

	// static $name = 'name';

	// static function doSomeThing(){
	// 	dd('doSomeThing');
	// }

	//用用戶輸入資料來建立一筆新的工作資訊
	public function saveTask(){
		$task = new Task();
		$task->title = 'title';
		$task->sub_title = 'sub_title';
		$task->content = 'content';
		$task->page_view = 0;
		$task->is_feature = true;
		$task->save();

		return redirect('/');
	}

	//回傳所指定task_id的資料列實例
	public function showTask($task_id){
		//如果資料不存在就會拋出例外
		$task = Task::findOrFail($task_id);
		return $task->title;
		//SQL語法 select * from tasks where id = $task_id
	}

	//用輸入資料來變更工作資料，但限定title.sub_title.content三欄位
	public function updateTask($id , $request){
		$task = Task::find($id);
		$task->update($request->only('title','sub_title','content'));
	}

	public function queryDemo(){

		//找到id為1的資料
		$task = Task::find(1);

		//找到id為1的資料，找不到就拋出錯誤
		$task = Task::findOrFail(1);

		//設定查詢條件，並回傳第一筆資料
		$task = Task::where('is_feature',true)->first();


		//取得tasks表單的所有資料
		$tasks = Task::all();
		$tasks = Task::get();//與上相同

		//設定查詢條件，並回傳所有符合的資料
		$tasks = Task::where('is_feature',true)->get();

		//設定查詢條件，並回傳所有符合的資料，再根據建立時間作反向排序
		$tasks = Task::where('is_feature',true)->orderBy('created_at','desc')->get();

	}

	//過濾出is_feature為true，且page_view大於0的所有資料
	public function scopeTopTasks($query){
		return $query->where('is_feature',true)->where('page_view','>',0);
	}

	//Accessor，取用page_view屬性前，先行處理
	public function getPageViewAttribute($value){
		return $value >0 ? $value : '沒人看';
	}

	//Mutator，設定cotent屬性前，先行處理
	public function setContentAttribute($value){
		$this->attributes['content'] = $value == null ? '沒有內容' : $value;
	}

	//一對一關係的屬性
	public function cgy(){
		return $this->belongsTo(Cgy::class);
	}

	//多對多關係
	public function users(){
		return $this->belongsToMany(User::class)->withTimestamps();
	}


}
