<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cgy_id')->unsigned()->index();
            //設定此欄位與cgys的id欄位之外鍵關係
            $table->foreign('cgy_id')->references('id')->on('cgys');
            $table->string('title',255);
            $table->string('sub_title',100);
            $table->longText('content');
            $table->integer('page_view');
            $table->boolean('is_feature');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function(Blueprint $table)
        { 
            //移除cgy_id欄位的外鍵關係
            $table->dropForeign(['cgy_id']);
        });
        Schema::dropIfExists('tasks');
    }
}
