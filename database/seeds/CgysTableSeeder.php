<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CgysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table("cgys")->delete();
        App\Cgy::truncate();
        // $faker = Faker::create("zh_TW");
        //建立一百筆資料
        // for ($i=0; $i < 100; $i++) { 
        // 	\App\Cgy::create([
	       //  	'title' => $faker->word,
	       //  	'desc' => $faker->sentence,
	       //  	'enabled' => rand(0,1),
	       //  ]);
        // }
        factory(\App\Cgy::class,10)->create();
        
    }
}
