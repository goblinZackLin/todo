<?php

use Illuminate\Database\Seeder;

class TaskUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table("task_user")->delete();
        App\TaskUser::truncate();
        factory(\App\TaskUser::class,1000)->create();
    }
}
