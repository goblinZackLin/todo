<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //關閉外鍵檢查
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(CgysTableSeeder::class);
        $this->call(TasksTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TaskUserTableSeeder::class);
        
        //開啟外鍵檢查
        Model::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
