<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table("tasks")->delete();
        App\Task::truncate();
        // $faker = Faker::create("zh_TW");
        //建立一百筆資料
        // for ($i=0; $i < 100; $i++) { 
        // 	App\Task::create([
	       //  	'title' => $faker->word,
	       //  	'sub_title' => $faker->word,
	       //  	'content' => $faker->sentence,
	       //  	'is_feature' => rand(0,1),
	       //  	'page_view' => rand(10,100),
	       //  ]);
        // }
        factory(\App\Task::class,800)->create();
    }
}
