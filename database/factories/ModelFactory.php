<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

//將Seeder的內容移到這裡...

$factory->define(App\Task::class, function (Faker\Generator $faker) {
    $ids = App\Cgy::pluck('id');
    return [
        'cgy_id' => $ids[rand(0,count($ids)-3)],
        'title' => $faker->word,
	    'sub_title' => $faker->word,
	    'content' => $faker->sentence,
	    'is_feature' => rand(0,1),
	    'page_view' => rand(10,100),
    ];
});

$factory->define(App\Cgy::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
	    'desc' => $faker->sentence,
	    'enabled' => rand(0,1),
    ];
});

$factory->define(App\TaskUser::class, function (Faker\Generator $faker) {
    $taskIds = App\Task::pluck('id');
    $userIds = App\User::pluck('id');
    return [
        'task_id' => $taskIds[rand(0,count($taskIds)-1)],
        'user_id' => $userIds[rand(0,count($userIds)-1)],
    ];
});
